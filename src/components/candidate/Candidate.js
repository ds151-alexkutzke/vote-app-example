import React from 'react';
import './Candidate.css';

const Candidate = ({name, votes, onVote}) => {
  return(
    <div className="candidate-box">
      <p>{name}</p>
      <p>{votes}</p>
      <button
        onClick={() => onVote(name)}
      >
        Vote!
      </button>
    </div>
  )
}

export default Candidate;
