import React, { useState } from 'react';
import Candidate from '../candidate/Candidate';

const Ballot = ({ initialCandidates }) => {
  const [candidates, setCandidates] = useState(initialCandidates)
  const [finished, setFinished] = useState(false)

  function onVote(candidateName) {
    setCandidates(prevCandidates => {
      //let newCandidates = [...prevCandidates];
      //let index = newCandidates.findIndex((candidate) => candidate.name === candidateName);
      //newCandidates[index].votes++;
      let newCandidates = prevCandidates.map(candidate => {
          const newObject = Object.assign({}, candidate)
          if(candidate.name === candidateName){
            newObject.votes = newObject.votes + 1 
          }  
          return newObject;
        });

      return(newCandidates);

    });
  }

  return (
    finished ?
    <div>
      {
        candidates.map((candidate) => {
          return (
            <p>{candidate.name}: {candidate.votes}</p>
          );
        })
      }
      <p>{[...candidates].sort((a,b) => +(a.votes < b.votes) || +(a.votes === b.votes) - 1)[0].name} é o vencedor!</p>
    </div>
    :
    <div>
      <div>
      {
        candidates.map((candidate) => {
          return (
            <Candidate
              name={candidate.name}
              votes={candidate.votes}
              onVote={onVote}
            />
          );
        })
      }
      </div>
      <div>
        <button onClick={() => setFinished(true)}>Finish!</button>
      </div>
    </div>
  )
}

export default Ballot;
