import Ballot from './components/ballot/Ballot';
import './App.css';

const candidates = [
  {
    name: "Marty McFly",
    votes: 0,
  },
  {
    name: "Doc",
    votes: 0,
  },
  {
    name: "Biff Tannen",
    votes: 0,
  },
];

function App() {
  return (
    <div className="App">
      <Ballot initialCandidates={candidates} />
    </div>
  );
}

export default App;
